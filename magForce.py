from visual import *

def magForceCharge(q, v, B):
    return cross(q*v, B)

def magForceWire(I, deltaL, B):
    return cross(I * deltaL, B)
