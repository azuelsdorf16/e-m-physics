# README #

### What is this repository for? ###

This is a repository for performing computations used in electrical and magnetic physics efficiently, such
as finding the electric field created by a charged particle. This is not meant to "automate" anyone's homework,
but these programs are probably less susceptible to careless errors than a student trying to finish homework
fifteen minutes before the deadline.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

To use this source code, you will need to have Python 2.x or 3.x
and the visual module installed. Since this could be installed in many ways
depending upon your operating system, this information will not be provided
at this time.

Once you have installed Python and the visual module, copy this source code
into your directory or add it to your path. Then, import it as you would any
other module. See example below.

Examples:

from efield import \*

from magfield import \*

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

If you choose to contribute, then please either write extensive test cases, have several
colleagues test your code, or perform some other actions as "insurance" that your code
works before submitting a pull request. Submitting your test cases will not be required,
but buggy code will be removed with or without notice, so please do not submit code after
running just a handful of test cases (trust me on this one). 

### Whom do I talk to? ###

* Repo owner or admin
* Other community or team contact