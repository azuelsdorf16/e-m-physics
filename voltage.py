def voltage(r1, r2, q1, q2, d):
    answer = (9 * 10 ** 9) * q1 * (1 / r1 - 1 / d)
    answer += q2 / ((3.14159265359 * r2 * r2) * (8.85 * 10 ** (-12))) * (r1 - d)
    return answer
