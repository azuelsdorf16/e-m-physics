from visual import *

mnofp = 1 * 10 ** -7 #Teslas * meters^2 / (Coulombs * meters / seconds)
pi = 3.14159265359

def magFieldCharge(q, v, r):
    rhat = r / r.mag
    return mnofp * cross((q * v), rhat) / r.mag2

def magFieldShortWire(I, deltaL, r):
    rhat = r / r.mag
    return mnofp * cross(I * deltaL, rhat) / r.mag2

def magFieldLongWire(L, I, r):
    return mnofp * L * I / (r.mag * sqrt(r.mag2 + (L / 2.0) ** 2))

def magFieldLongWireApprox(I, r):
    return mnofp * 2.0 * I / r.mag

def magFieldLoop(R, I, z):
    return mnofp * 2.0 * pi * (R ** 2) * I / (sqrt(z ** 2 + R ** 2) ** 3)

def magFieldLoopApprox(R, I, z):
    return mnofp * 2.0 * pi * (R ** 2) * I / (z ** 3)
