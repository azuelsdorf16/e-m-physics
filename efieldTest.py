from efield import *

def main():
    q1 = -1.6 * 10 ** -19
    q2 = -1.6 * 10 ** (-19)
    r1 = vector(6.0 * 10 ** (-9), -2.0 * 10 ** (-9), -7.0 * 10 ** (-9))
    r2 = vector(-4.0 * 10 ** (-9), 2.0 * 10 ** (-9), 3.0 * 10 ** (-9))
    print (efield(r2, r1, q1))
    print (force(q2, r2, r1, q1))

if __name__ == "__main__":
    main()
