L = 1 #m
steps = 100 #number of pieces we have partitioned the rod into
Q = 1 * 10 ** (-9) #1 nC
COULOMB_CONSTANT = 9 * 10 ** 9 #Nm^2/(C^2)
x = 0.05
deltaY = 1. * L / steps

def main():
    y = -(L / 2.0 - L / (2.0 * steps))
    summation = 0

    while y < (L / 2.0):
        Ex = (1. * Q / L * x)
        Ex /= ((x ** 2 + y ** 2) ** (3 / 2.))
        summation += Ex
        y = y + deltaY

    Ex = summation * deltaY * COULOMB_CONSTANT
    print (Ex)
    

if __name__ == "__main__":
    main()
